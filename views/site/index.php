<?php
use yii\widgets\LinkPager;

/* @var $this yii\web\View */

$this->title = 'Products';
?>

<div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">Panel heading</div>
    <div class="panel-body">
        <p>...</p>
    </div>

    <!-- Table -->
    <table class="table">
        <tr>
            <th>Имя</th>
            <th>Цена</th>
            <th>Категория</th>
        </tr>
        <?php
        if(count($products)) {
            foreach ($products as $product) {
                ?>
                <tr>
                    <td><?php echo $product['name']; ?></td>
                    <td><?php
                        if($product['old_price'] > 0) {
                            echo '<span style="text-decoration:line-through">'.$product['old_price'].'</span> ';
                        }
                        echo $product['price'];
                        ?></td>
                    <td><?php
                        if(count($product['categories'])) {
                            $br = false;
                            foreach($product['categories'] as $categoryInfo) {
                                if(isset($categories[$categoryInfo['category_id']])) {
                                    echo $categories[$categoryInfo['category_id']];

                                }
                                else {
                                    echo 'Нет информации о категории с ID:'.$categoryInfo['category_id'];
                                }
                                if(!$br) {
	                                $br = true;
                                }
                                else {
	                                echo '<br>';
                                }
                            }
                        }
                    ?></td>
                </tr>
                <?php
            }
        }
        ?>
    </table>

    <?php
    echo LinkPager::widget([
	    'pagination' => $pages,
    ]);
    ?>
</div>