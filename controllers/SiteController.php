<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Products;
use app\models\Categories;
use yii\data\Pagination;

class SiteController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}


	/**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
    	// Получаем все товары с категориями
	    $query = Products::find()->with('categories')->select(['id', 'name', 'price', 'old_price']);

	    $countQuery = clone $query;
	    $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);
	    // приводим параметры в ссылке к ЧПУ
	    $pages->pageSizeParam = false;
	    $products = $query->offset($pages->offset)
	                    ->limit($pages->limit)
	                    ->asArray()->all();

	    // Получаем все категории
	    $categoriesData = Categories::find()->select(['id', 'name'])->asArray()->all();
	    // Меняем формат массива в виде [category_id => category_name]
	    $categories = [];
	    foreach ($categoriesData as $category) {
	    	$categories[$category['id']] = $category['name'];
	    }

        return $this->render('index', compact(['products', 'pages', 'categories']));
    }
}
